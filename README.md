# LV

A small Lotka-Volterra simulation tool I wrote to get acquainted with Perl-Tk (prelude to the GUI for [StochSim](https://gitlab.com/ngamblen/StochSim)).

Very simple and naive. It uses forward Euler for system update (I know...) and I never coded the phase portrait part. If you want to model for fun or for work, have a look at the awesome [COPASI](http://copasi.org/)
