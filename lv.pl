#!/usr/bin/perl

###############################################################################
#   lv, a perl/Tk programme to simulate the Lotka-Volterra predator-prey model
#              
# File: lv.pl
#
# Aim:  simulation of Lotka-Volterra reactions
# A+X->2X; k1
# X+Y->2Y; k2
# Y->Z; k3
# (initially A was the grass, X the rabbits, Y the lynx, Z the dead lynx ...)
#
# Author: Nicolas Gambardella
# (c) 2000
# Date of last modification: 07/MAR/2024
#
###############################################################################
#   This program is free software; you can redistribute it and/or modify it 
#   under the terms of the GNU General Public License as published by the Free 
#   Software Foundation; either version 2 of the License, or (at your option) 
#     any later version.
#
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
#   for more details.
#
#   You should have received a copy of the GNU General Public License along 
#   with this program; if not, write to the Free Software Foundation, Inc., 59 
#   Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Nicolas Gambardella
#   e-mail: nicgambarde@gmail.com

use 5.005;
use strict;
use Tk 800.022;
use vars qw/$VERSION $a_init $x_init $y_init $k1 $k2 $k3 $v $d $t $width $height $minval $maxval $pr/;
use Tk::widgets qw/LabFrame LabEntry/;

my $Na = 6.0225e23;
$VERSION = "0.001";
# Defaults. Have to be meaningful
$a = 1000;
$x_init = 1000;
$y_init = 1000;
$k1 = 2500;
$k2 = 2500;
$k3 = 5000;
$v = 1e-21;
$d = 1e-2;
$t = 1e-06;
$width = 500;
$height = 300;
$minval = 500;
$maxval = 5000;
$pr = 100;

# FIXME: separation between time-slice and print. 
# Not forced to dump everything.

my $mw = MainWindow->new();
$mw->title("Lotka-Volterra");

my $setup_Fr = $mw->LabFrame(-borderwidth => 1, 
			     -label       => 'Set up the simulation',
			     -labelside   => 'acrosstop'
			     )->pack(-expand => '1', 
				     -fill   => 'both');

my $bottom_Fr = $setup_Fr->LabFrame(-borderwidth => 1, 
				    -label       => 'simulation parameters',
				    -labelside   => 'acrosstop'
				    )->pack(-side => 'bottom',
					    -expand => '1', 
					    -fill   => 'both');

$bottom_Fr->LabEntry(-label        => 'volume',
		    -textvariable => \$v)->pack;
$bottom_Fr->LabEntry(-label        => 'duration',
		    -textvariable => \$d)->pack;
$bottom_Fr->LabEntry(-label        => 'tslice',
		     -textvariable => \$t)->pack;
$bottom_Fr->LabEntry(-label        => 'print ratio',
		     -textvariable => \$pr)->pack;
$bottom_Fr->LabEntry(-label        => 'minimum Y',
		     -textvariable => \$minval)->pack;
$bottom_Fr->LabEntry(-label        => 'maximum Y',
		     -textvariable => \$maxval)->pack;
my $left_Fr = $setup_Fr->LabFrame(-borderwidth => 1, 
				  -label       => 'Initial amount',
				  -labelside   => 'acrosstop'
				  )->pack(-side => 'left');

$left_Fr->LabEntry(-label        => 'A',
		   -textvariable => \$a)->pack;
$left_Fr->LabEntry(-label        => 'X',
		   -textvariable => \$x_init)->pack;
$left_Fr->LabEntry(-label        => 'Y',
		   -textvariable => \$y_init)->pack;

my $right_Fr = $setup_Fr->LabFrame(-borderwidth => 1, 
				   -label       => 'Reaction rates',
				   -labelside   => 'acrosstop'
				   )->pack(-side => 'right');

$right_Fr->LabEntry(-label        => 'k1',
		    -textvariable => \$k1)->pack;
$right_Fr->LabEntry(-label        => 'k2',
		    -textvariable => \$k2)->pack;
$right_Fr->LabEntry(-label        => 'k3',
		    -textvariable => \$k3)->pack;



my $action_Fr = $mw->Frame()->pack(-expand => '1', 
				   -fill   => 'x');

my $run_Bt = $action_Fr->Button(-text    => 'X,Y Vs. t',
				-command => sub{ XYversusT() }
				)->pack(-side => 'left');

my $run_Bt = $action_Fr->Button(-text    => 'X Vs. Y',
				-command => sub{ XversusY() }
				)->pack(-side => 'left');

# IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT IMPORTANT
# Note that tk exit function is used here instead of
# -command => [$main => 'destroy']
# The code after the MainLoop is therefore ignored.
# Remind it if you add code after the GUI termination

my $quit_Bt = $action_Fr->Button(-text             => "quit",
				 -command          => sub{ exit },
				 )->pack(-side => 'right');

MainLoop;

sub XYversusT{
    my $c;
    
    # Define the axis. I leave space for labels and legends
    my %axis = ("minabs" => 50,
		"maxabs" => $width - 50,
		"minord" => $height - 30,
		"maxord" => 10,
		);
    $axis{"width"} = abs( $axis{"maxabs"} - $axis{"minabs"} );
    $axis{"height"} = abs( $axis{"maxord"} - $axis{"minord"} );
    # DO NOT FORGET: THE Y ORIGIN IS ON THE TOP

    if(!Exists(my $plot_W)){
	$plot_W = $mw->Toplevel();
	$plot_W->title("X,Y versus t");
	$c = $plot_W->Scrolled('Canvas',	
			       -width  => $width,
			       -height => $height,
			       )->pack(-side => 'bottom');
	my $close_Bt = $plot_W->Button(-text    => "close",
				       -command => sub{ $plot_W->destroy }
				       )->pack(-side => 'left');
	my $print_Bt = $plot_W->Button(-text    => "print",
				       -command => sub { 
					   my $psfile;
					   $psfile = $mw->getSaveFile
					       (-title       => "Save plot as",
						-initialfile => "plot.ps" );
					   $c->postscript(-file => $psfile);
				       }
				       )->pack(-side => 'left');

#------------------------------ The axis -------------------------------------------------------------
	
	$c->createLine($axis{"minabs"}, 
		       $axis{"minord"}, 
		       $axis{"minabs"}, 
		       $axis{"maxord"}, 
		       -width => 2);
	$c->createLine($axis{"minabs"}, 
		       $axis{"minord"}, 
		       $axis{"maxabs"}, 
		       $axis{"minord"}, 
		       -width => 2);
	
#----------------------------- The labels ------------------------------------------------------------
# They have to change with the progression. See the function updateplot. Only the
# marks stay there.
	for my $i (1..10){
	    $c->createLine($axis{"minabs"},
			   $axis{"minord"} - $axis{"height"} / 10 * $i, # minus because Y VERS LE BAS
			   $axis{"minabs"} - 5,
			   $axis{"minord"} - $axis{"height"} / 10 * $i,
			   );
	}
	for my $i (1..5){
	    $c->createLine($axis{"minabs"} + $axis{"width"} / 5 * $i,
			   $axis{"minord"},
		       $axis{"minabs"} + $axis{"width"} / 5 * $i,
			   $axis{"minord"} + 5,
			   );
	}
	
#--------------------------- The legend --------------------------------------------------------------
	
	$c->createLine( $axis{"maxabs"}+2, 
			$axis{"maxord"}, 
			$axis{"maxabs"}+10,
			$axis{"maxord"},
			-width => 2,
			-fill  => "red" );
	
	$c->createText( $axis{"maxabs"}+12, 
			$axis{"maxord"}, 
			-text   => 'X',
			-anchor => 'w',
			);
	
	$c->createLine( $axis{"maxabs"}+2, 
			$axis{"maxord"}+12, 
			$axis{"maxabs"}+10,
			$axis{"maxord"}+12,
			-width => 2,
			-fill  => "blue");
	
	$c->createText( $axis{"maxabs"}+12, 
			$axis{"maxord"}+12, 
			-text   => 'Y',
			-anchor => 'w',
			);
	
#------------------------------------------- Tracing the label of values -----------------------------
	
	my $val_elt = int( ($maxval -$minval) / 10 );
	
	for my $i (1 .. 11){
	    $c->createText($axis{"minabs"}-6, 
			   $axis{"minord"}-$axis{"height"}/10*$i,
			   -text   =>  $minval + $val_elt * $i,
			   -anchor => 'e',
			   );
	}
	
	my $time_elt = $d / 5; # not int, because time can be inferior to 1 second
	
	for my $i (1 .. 11){
	    $c->createText($axis{"minabs"}+$axis{"width"}/5*$i, 
			   $axis{"minord"}+6,
			   -text   =>  sprintf("%.1e",$time_elt * $i),
			   -anchor => 'n',
			   );
	}
	
#	$c->configure(-scrollregion => [ $c->bbox("all") ]);	

	my $ca = $a/($v * $Na);
	my $x = $x_init;
	my $cx = $x_init/($v * $Na);
	my $y = $y_init;
	my $cy = $y_init/($v * $Na);
	my $dcx = undef;
	my $dcy = undef;
	print "$x $y\n";
	for my $i (0 .. ($d/$t)){

	    if (not $i % $pr){ 

		$c->createOval($axis{"minabs"} + $i * $t *  $axis{"width"} / $d -1, 
			       $axis{"minord"} - (($x - $minval) * $axis{"height"} / ($maxval - $minval)) -1,
			       $axis{"minabs"} + $i * $t *  $axis{"width"} / $d +1, 
			       $axis{"minord"} - (($x - $minval) * $axis{"height"} / ($maxval - $minval)) +1,
			       -fill    => 'red',
			       -outline => 'red',
			       );
		$c->createOval($axis{"minabs"} + $i * $t *  $axis{"width"} / $d -1, 
			       $axis{"minord"} - (($y - $minval) * $axis{"height"} / ($maxval - $minval)) -1,
			       $axis{"minabs"} + $i * $t *  $axis{"width"} / $d +1, 
			       $axis{"minord"} - (($y - $minval) * $axis{"height"} / ($maxval - $minval)) +1,
			       -fill    => 'blue',
			       -outline => 'blue',
			       );
		$c->update;
		print "$x $y\n";
	    }
# A + X -> 2X; k1
# X + Y -> 2Y; k2
#   Y   ->  Z; k3
	    $dcx = ( $k1 * $ca * $cx - $k2 * $cx * $cy ) * $t;
	    $dcy = ( $k2 * $cx * $cy - $k3 * $cy ) * $t; 
	    $cx += $dcx;
	    $x = $cx * ($v * $Na);
	    $cy += $dcy;
	    $y = $cy * ($v * $Na);
	}
	
    } else {
	$plot_W->deiconify();
	$plot_W-> raise();
    }

}

sub XversusY{
    print "Phase portrait not yes implemented...\n";
}


